import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FlatMapclass {

    public static void main(String[] args) {

        Employee emp1 = new Employee();
        emp1.setName("Rambha");
        emp1.addDept("IT");
        emp1.addDept("Finance");
        emp1.addDept("HR");

        Employee emp2 = new Employee();
        emp2.setName("Menaka");
        emp2.addDept("IT");
        emp2.addDept("Heaven");
        emp2.addDept("Enna racala");

        List<Employee> employees = new ArrayList<>();
        employees.add(emp1);
        employees.add(emp2);

        Stream<Set<String>> empMaps = employees.stream().map(x -> x.getDept());
//        empMaps.forEach(x -> System.out.println(x));
        List<String> strings = empMaps.flatMap(x -> x.stream()).distinct().collect(Collectors.toList());

        strings.forEach(x -> System.out.println(x));
    }

}

  class Employee {
     private String name;
     private Set<String> dept;

     public void addDept(String dept) {

         if(this.dept == null)
         {
             this.dept = new HashSet<>();
         }
         this.dept.add(dept);
     }
     public String getName() {
         return name;
     }

     public void setName(String name) {
         this.name = name;
     }

     public Set<String> getDept() {
         return dept;
     }

     public void setDept(Set<String> dept) {
         this.dept = dept;
     }
 }