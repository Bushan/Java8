import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class MapFilters {
    public static void main(String[] args) {
        Map<Integer, String> filters =  new HashMap<>();
        filters.put(1, "Filter1");
        filters.put(2, "Filter2");
        filters.put(3, "Filter3");
        filters.put(4, "Filter4");

        String filtereds = filters.entrySet().stream().filter(
                x -> "Filter1".equals(x.getValue())
        ).map(x -> x.getValue()).collect(Collectors.joining());
        System.out.println("filter :: "+filtereds);

        Map<Integer,String> filter2 = filters.entrySet().stream().filter(
                x -> "Filter2".equals(x.getValue())
        ).collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
        System.out.println("filter 2 : "+filter2);
    }
}
