import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MapFiltersPredicate {
    public static void main(String[] args) {

        Map<Integer, String> map1 = new HashMap<>();
        map1.put(1, "London");
        map1.put(2, "Manchester");
        map1.put(3, "Leeds");
        map1.put(4,"Birmingham");
        map1.put(5,"chester");

        Map<Integer,String> preValue = filterByValue(map1, x -> x.contains("chester"));
        System.out.println("pre value :: "+preValue);

        Map<Integer,String> preValue1 = filterByValue(map1, x -> x.startsWith("L"));
        System.out.println("prevalue1" +preValue1);


        Map<Integer,String> preValue2 = filterByValue(map1, x -> (x.contains("Le") || x.contains("Bi")));
        System.out.println("prevalue2" +preValue2);
    }

    public static <K,V> Map<K,V> filterByValue(Map<K,V> map, Predicate<V> predicate)
    {
        return map.entrySet().stream().filter(
                x -> predicate.test(x.getValue())
        ).collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
    }
}
