import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class JavaGroupBy {

    public static void main(String[] args) {
        List<String> items = Arrays.asList("unitedkingdom", "india", "unitedstates", "unitedkingdom", "malasya", "malasya", "malasya", "india", "india", "india", "unitedkingdom");

        Map<String, Long> map = items.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println(" results :: " + map);

        Map<String, Long> orderedMap = new LinkedHashMap<>();
        map.entrySet().stream().sorted(Map.Entry.<String, Long>comparingByKey().reversed()).forEachOrdered(e -> orderedMap.put(e.getKey(), e.getValue()));

        System.out.println(orderedMap);


        new JavaGroupBy().groupByObject();
    }

    // Group by using the custom object
    public void groupByObject() {

        List<Values> valuesList = Arrays.asList(new Values("Leeds", 2, new BigDecimal(100)),
                new Values("Leeds", 3, new BigDecimal(100)),
                new Values("Manchester", 6, new BigDecimal(100)),
                new Values("London", 10, new BigDecimal(1000)),
                new Values("Manchester", 3, new BigDecimal(350)),
                new Values("Manchester", 100, new BigDecimal(1000)),
                new Values("Leeds", 100, new BigDecimal(350)),
                new Values("Manchester", 7, new BigDecimal(100)),
                new Values("London", 9, new BigDecimal(600)));

        Map<String, Integer> result = valuesList.stream().collect(
                Collectors.groupingBy(Values::getName, Collectors.summingInt(Values::getQty))
        );

        Map<BigDecimal, Set<String>> sets = valuesList.stream().collect(
                Collectors.groupingBy(Values::getPrice, Collectors.mapping(Values::getName, Collectors.toSet()))
        );

        Map<BigDecimal,List<Values>> pricegroups = valuesList.stream().collect(Collectors.groupingBy(Values::getPrice));
        System.out.println("Price groups :: " +pricegroups);
        System.out.println("results :: "+result);
        System.out.println("sets :: "+sets);
    }

    class Values {
        private String name;
        private int qty;
        private BigDecimal price;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getQty() {
            return qty;
        }

        public void setQty(int qty) {
            this.qty = qty;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public void setPrice(BigDecimal price) {
            this.price = price;
        }

        public Values(String name, int qty, BigDecimal price) {
            this.name = name;
            this.qty = qty;
            this.price = price;
        }

        @Override
        public String toString() {
            return "Values{" +
                    "name='" + name + '\'' +
                    ", qty=" + qty +
                    ", price=" + price +
                    '}';
        }
    }
}

