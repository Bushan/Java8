import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class OtherObjectList {
    public static void main(String[] args) {

        List<Person> personList = Arrays.asList(new Person("Ram", 34),
                new Person("Lak", 32),
                new Person("Hanuma", 30));

        List<PersonObject> personObjectList = personList.stream().map(temp -> {
           PersonObject personObject = new PersonObject();
           personObject.setName(temp.getName());
           personObject.setAge(temp.getAge());
           return personObject;
        }
        ).collect(Collectors.toList());
        System.out.println(personObjectList);
    }
}
