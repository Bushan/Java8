import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class EliminateNulls {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("ram","lak",null,"hanuma");

        List<String> uniqueList = names.stream().filter(x -> x!=null).collect(Collectors.toList());
        System.out.println(uniqueList);
    }
}
