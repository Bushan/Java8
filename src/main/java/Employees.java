import java.math.BigDecimal;

public class Employees {

    private String name;
    private Integer salary;
    private int id;

    public Employees(String name, Integer salary, int id)
    {
        this.name=name;
        this.salary=salary;
        this.id=id;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.name+" "+ this.salary + " " + this.id;
    }
}
