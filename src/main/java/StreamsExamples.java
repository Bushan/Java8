import com.sun.deploy.perf.PerfRollup;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsExamples {
    public static void main(String[] args) {

        List<Person> personList = Arrays.asList(new Person("Ram", 32),
                new Person("Lak", 30),
                new Person("Hanuma", 28)
                );

       Person person =  personList.stream()
                .filter(x -> "Lak".equals(x.getName()))
                .findAny()
                .orElse(null);
       System.out.println(person.toString());

       List<String> names = personList.stream().map(Person::getName).collect(Collectors.toList());
       System.out.println(names);
    }
}
