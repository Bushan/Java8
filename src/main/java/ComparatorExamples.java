import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ComparatorExamples {

    public static void main(String[] args) {
        List<Employees> employeesList = getEmployees();

        System.out.println("Before Sort");
        for (Employees employees : employeesList) {
            System.out.println(employees);
        }

        //Sort by age
//        Collections.sort(employeesList, new Comparator<Employees>() {
//            public int compare(Employees o1, Employees o2) {
//                return o1.getName().compareTo(o2.getName());
//            }
//        });
//        employeesList.sort(new Comparator<Employees>() {
//            public int compare(Employees o1, Employees o2) {
//                return o1.getName().compareTo(o2.getName());
//            }
//        });

        employeesList.sort((Employees o1, Employees o2)->o1.getId()-o2.getId());

        System.out.println("After Sort");
        employeesList.forEach(employees -> System.out.println(employees));
        Comparator<Employees> employeesComparator = (o1,o2) -> o1.getSalary().compareTo(o2.getSalary());
        employeesList.sort(employeesComparator.reversed());
        System.out.println("reverse order");
        employeesList.forEach(employees -> System.out.println(employees));

//        System.out.println("After Sort");
//        for (Employees employees : employeesList) {
//            System.out.println(employees);
//        }

    }
    private static List<Employees> getEmployees() {

        List<Employees> result = new ArrayList<Employees>();

        result.add(new Employees("mkyong", 70000, 33));
        result.add(new Employees("alvin", 80000, 20));
        result.add(new Employees("jason", 100000, 10));
        result.add(new Employees("iris", 170000, 55));

        return result;

    }
}
