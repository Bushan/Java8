import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FlatMapIntClass {
    public static void main(String[] args) {
        int[] values = {1,2,3,4,5};

        Stream<int[]> streams = Stream.of(values);

        IntStream stream = streams.flatMapToInt(x->Arrays.stream(x));
        stream.forEach(x->System.out.println(x));
    }
}
