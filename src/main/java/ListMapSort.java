import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ListMapSort {
    public static void main(String[] args) {

        ListMapSort listMapSort = new ListMapSort();
        listMapSort.listToMap();

        }

    public void listToMap() {
        List<Samples> list = new ArrayList<>();
        list.add(new Samples(1, "liquidweb.com", 80000));
        list.add(new Samples(2, "linode.com", 90000));
        list.add(new Samples(3, "digitalocean.com", 120000));
        list.add(new Samples(4, "aws.amazon.com", 200000));
        list.add(new Samples(5, "mkyong.com", 1));
        list.add(new Samples(6, "linode.com", 100000));
        list.add(new Samples(6, "lin.com", 100000));

        Map<Integer, String> mapValues = list.stream().collect(
                Collectors.toMap(Samples::getId,Samples::getName, (oldValue1, newValue1) -> newValue1));
        System.out.println(mapValues);

    }

    class Samples {
        private int id;
        private String name;
        private long numbers;

        public Samples(int id, String name, long numbers) {
            this.id=id;
            this.name=name;
            this.numbers = numbers;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public long getNumbers() {
            return numbers;
        }

        public void setNumbers(long numbers) {
            this.numbers = numbers;
        }

        @Override
        public String toString() {
            return "Samples{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", numbers=" + numbers +
                    '}';
        }
    }
}
